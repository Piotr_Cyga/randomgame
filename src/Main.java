public class Main {
    public static void main(String[] args) {
        Lotto duzyLotek = new Lotto();
        boolean skreslenieSzostki = false;
        int licznik = 0;
        int trafienia = 0;
        while (skreslenieSzostki == false) {
            duzyLotek.losowanie();
            duzyLotek.skreslanieKuponu();
            trafienia = duzyLotek.sprawdzenie();
            licznik++;
            System.out.println(licznik);
            if (trafienia == 6) {
                skreslenieSzostki = true;
                System.out.println("trafiony");
            }
        }
    }
}
