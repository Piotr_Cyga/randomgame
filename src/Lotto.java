import java.util.Random;

public class Lotto {
    private int[] wyniki = new int [6];
    private int[] kupon = new int [6];

    public int[] getWyniki() {
        return wyniki;
    }

    public int[] getKupon() {
        return kupon;
    }

    public void losowanie () {
        Random rd = new Random();
        System.out.println("oto wyniki dzisiejszego losowania: ");
        for (int i = 0; i<wyniki.length; i++) {
        wyniki [i] = rd.nextInt(39) + 1;
            for (int j = 0; j < i; j++) {
                while (wyniki[j] == wyniki[i]) {
                    wyniki [i] = rd.nextInt(39) + 1;
                }
            }
        }
        sort(wyniki);
        podaj(wyniki);
        System.out.println("\n");
    }
    public void skreslanieKuponu () {
        Random rd = new Random();
        for (int i = 0; i<kupon.length; i++) {
            kupon [i] = rd.nextInt(39) + 1;
            for (int j = 0; j <i ; j++) {
                while (kupon[j] == kupon[i]) {
                    kupon [i] = rd.nextInt(39) + 1;
                }
            }
        }
        sort(kupon);
        podaj(kupon);
        System.out.println("\n");
    }
    public void sort (int [] tablica) {
        for (int i = (tablica.length - 1); i >= 0; i--)
        {
            for (int j = 1; j <= i; j++)
            {
                if (tablica[j-1] > tablica[j])
                {
                    int temp = tablica[j-1];
                    tablica[j-1] = tablica[j];
                    tablica[j] = temp;
                }
            }
        }
    }
    public int sprawdzenie () {
        int trafienia = 0;
        for (int i = 0; i<wyniki.length; i++ ) {
            if (wyniki[i] == kupon [i]) {
                trafienia++;
                System.out.println("trafione " + trafienia + " liczb.");
            }
        }
        return trafienia;
    }
    public void podaj (int[] tablica) {
        System.out.print(" Wynik " + tablica + ":");
        for (int i = 0; i<tablica.length; i++) {
            System.out.print( tablica[i] + ", ");
        }
    }
}
